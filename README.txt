
ANNIVERSARY MODULE
------------------

This module is intended to enable Date Fields for views in a form that projects
them to the actual or next year.

That means any date in that field will be shown with its own month and day but
with the year either being the actual or the next depending on the comparision
of the field's and the actual date.

The anniversary module uses "standard" Date Fields and presents them to you as
corresponding Views fields, filters and sorts. The anniversary module also contains
Views fields, filters and sorts for the age at that date.

FEATURES
--------

 * For each Date Field:
    - a views field, filter and sort criteria "Field (next anniversary)"
    - a views field, filter and sort criteria "Fiels (age at next anniversary)"
 * For each of those you can:
    - set an offset that moves the year of scope in days (forwards or backwards)
      to enable for example a view to include the last three days.
    - use the features you know of normal Date / numeric Fields in views

FEATURES TO COME
----------------

 * Enable the Date Formatters for the next anniversary field

EXAMPLE
-------
You have a database of your friends and want a list of upcoming birthday. The
dates are saved as Date Fields named "date of birth":

John - 28th August 1979
Fred - 10th November 1989
Wolf - 27th January 1991

Then you can create a View of your friends and will find the two fields called
"date of birth (next anniversary)" and "date of birth (age at next anniversay)"
in the content group. Assuming you take a look at that view at 10th July 2011
and have it sorted by the next anniversary, it could look like this:

 * 28th August 2011
   John (32)
 * 10th November 2011
   Fred (22)
 * 27th January 2012
   Wolf (20)
