<?php
/**
 * Class anniversary_handler_filter_age
 */
class anniversary_handler_filter_age extends views_handler_filter_numeric {

  /**
   * Overrides views_handler_filter_date::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['start_offset'] = array(
      'default' => 0,
    );

    return $options;
  }

  /**
   * Overrides views_handler_filter_date::option_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['start_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset in days'),
      '#description' => t('When to start the year of scope, e.g. -1 = yesterday, ...'),
      '#fieldset' => 'more',
      '#default_value' => $this->options['start_offset'],
    );

  }
  
  /**
   * Overrides views_handler_filter_date::op_simple().
   */
  function op_simple($field) {
    $value = intval(strtotime($this->value['value'], 0));
    if (!empty($this->value['type']) && $this->value['type'] == 'offset') {
      $value = '***CURRENT_TIME***' . sprintf('%+d', $value); // keep sign
    }

    $now = getdate(time() + 24 * 60 * 60 * intval($this->options['start_offset']));
    $real_field = $this->real_field;
    $formula  = "CASE YEAR($real_field) = 0 WHEN TRUE THEN 0 $this->operator $value ELSE";
    $formula .= " CASE MONTH($real_field) > {$now['mon']} OR MONTH($real_field) = {$now['mon']} AND DAY($real_field) >= {$now['mday']}";
    $formula .= " WHEN TRUE THEN {$now['year']} - YEAR($real_field) $this->operator $value";
    $formula .= " ELSE {$now['year']}+1 - YEAR($real_field) $this->operator $value END";
    $formula .= " END";

    // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when using an offset.
    $this->query->add_where_expression($this->options['group'], "$formula");
  }
  
  /**
   * Overrides views_handler_filter_date::op_between().
   */
  function op_between($field) {
    $a = intval(strtotime($this->value['min'], 0));
    $b = intval(strtotime($this->value['max'], 0));

    if ($this->value['type'] == 'offset') {
      $a = '***CURRENT_TIME***' . sprintf('%+d', $a); // keep sign
      $b = '***CURRENT_TIME***' . sprintf('%+d', $b); // keep sign
    }

    $operator = strtoupper($this->operator);

    $now = getdate(time() + 24 * 60 * 60 * intval($this->options['start_offset']));
    $real_field = $this->real_field;
    $formula  = "CASE YEAR($real_field) = 0 WHEN TRUE THEN 0 $operator $a AND $b ELSE";
    $formula .= " CASE MONTH($real_field) > {$now['mon']} OR MONTH($real_field) = {$now['mon']} AND DAY($real_field) >= {$now['mday']}";
    $formula .= " WHEN TRUE THEN {$now['year']} - YEAR($real_field) $operator $a AND $b";
    $formula .= " ELSE {$now['year']}+1 - YEAR($real_field) $operator $a AND $b END";
    $formula .= " END";

    // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when using an offset.
    $this->query->add_where_expression($this->options['group'], "$formula");
  }
  
}
