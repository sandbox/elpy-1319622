<?php
/**
 * Class anniversary_handler_sort_age
 */
class anniversary_handler_sort_age extends views_handler_sort {

  /**
   * Overrides views_handler_sort::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['start_offset'] = array(
      'default' => 0,
    );

    return $options;
  }

  /**
   * Overrides views_handler_sort::option_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['start_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset in days'),
      '#description' => t('When to start the year of scope, e.g. -1 = yesterday, ...'),
      '#fieldset' => 'more',
      '#default_value' => $this->options['start_offset'],
    );

  }

  /**
   * Overrides views_handler_sort::query().
   */
  function query() {
    $this->ensure_my_table();

    if (!isset($this->query->fields[$this->field])) {
      // the anniversary field will not be loaded by itself, we have to do it:
      // $table must be NULL for the formula in $field to work
      $table = NULL;
      // the next lines are straight from anniversary_handler_field::query()
      $now = getdate(time() + 24 * 60 * 60 * intval($this->options['start_offset']));
      $real_field = $this->real_field;
      $field  = "CASE YEAR($real_field) = 0 WHEN TRUE THEN 0 ELSE";
      $field .= " CASE MONTH($real_field) > {$now['mon']} OR MONTH($real_field) = {$now['mon']} AND DAY($real_field) >= {$now['mday']}";
      $field .= " WHEN TRUE THEN {$now['year']} - YEAR($real_field)";
      $field .= " ELSE {$now['year']}+1 - YEAR($real_field) END";
      $field .= " END";
    }
    else {
      // field is present, take it:
      $table = $this->table;
      $field = NULL;
    }

    // alias is the $this->field anyway
    $alias = $this->field;
    // take order from options
    $order = $this->options['order'];

    // add the ORDER BY statement:
    $this->query->add_orderby($table, $field, $order, $alias);
  }
}