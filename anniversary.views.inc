<?php
/**
 * @file
 * Views API implementations of anniversary module
 */

/**
 * Implements hook_field_views_data_alter()
 */
function anniversary_field_views_data_alter(&$data, &$field, &$module) {
  if ($module == 'date') {

    $name = $field['field_name'];

    $data['field_data_' . $name][$name . '_anniversary'] = array(
      'group' => $data['field_data_' . $name][$name]['group'],
      'title' => t('@field_title (next anniversary)', array('@field_title' => $data['field_data_' . $name][$name]['title'])),
      'help' => $data['field_data_' . $name][$name]['help'],
      'real field' => $name . '_value',
      'field' => array(
        'handler' => 'anniversary_handler_field',
        'field_name' => $name,
        'table' => 'field_data_' . $name,
        'is date' => TRUE,
      ),
      'sort' => array(
        'handler' => 'anniversary_handler_sort',
        'field_name' => $name,
        'table' => 'field_data_' . $name,
      ),
      'filter' => array(
        'handler' => 'anniversary_handler_filter',
        'field_name' => $name,
        'table' => 'field_data_' . $name,
      ),
    );

    $data['field_data_' . $name][$name . '_anniversary_age'] = array(
      'group' => $data['field_data_' . $name][$name]['group'],
      'title' => t('@field_title (age at next anniversary)', array('@field_title' => $data['field_data_' . $name][$name]['title'])),
      'help' => $data['field_data_' . $name][$name]['help'],
      'real field' => $name . '_value',
      'field' => array(
        'handler' => 'anniversary_handler_field_age',
        'field_name' => $name,
        'table' => 'field_data_' . $name,
      ),
      'sort' => array(
        'handler' => 'anniversary_handler_sort_age',
        'field_name' => $name,
        'table' => 'field_data_' . $name,
      ),
      'filter' => array(
        'handler' => 'anniversary_handler_filter_age',
        'field_name' => $name,
        'table' => 'field_data_' . $name,
      ),
    );

  }
}

