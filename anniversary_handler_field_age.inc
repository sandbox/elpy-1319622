<?php
/**
 * @file
 * Class anniversary_handler_field_age
 */
class anniversary_handler_field_age extends views_handler_field {

  /**
   * Overrides views_handler_field::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    
    $options['start_offset'] = array(
      'default' => 0,
    );
    
    return $options;
  }

  /**
   * Overrides views_handler_field::option_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['start_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset in days'),
      '#description' => t('When to start the year of scope where -1 = yesterday, 0 = today, ...'),
      '#fieldset' => 'more',
      '#default_value' => $this->options['start_offset'],
    );
    
  }
  
  /**
   * Overrides views_handler_field::query().
   */
  function query() {
    $this->ensure_my_table();

    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    
    $now = getdate(time() + 24 * 60 * 60 * intval($this->options['start_offset']));
    $real_field = $this->real_field;
    $field  = "CASE YEAR($real_field) = 0 WHEN TRUE THEN 0 ELSE";
    $field .= " CASE MONTH($real_field) > {$now['mon']} OR MONTH($real_field) = {$now['mon']} AND DAY($real_field) >= {$now['mday']}";
    $field .= " WHEN TRUE THEN {$now['year']} - YEAR($real_field)";
    $field .= " ELSE {$now['year']}+1 - YEAR($real_field) END";
    $field .= " END";
    
    $alias = $this->field . '_age';
    
    $this->field_alias = $this->query->add_field(NULL, $field, $alias, $params);
  }

}