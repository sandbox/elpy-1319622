<?php
/**
 * Class anniversary_handler_field
 */
class anniversary_handler_filter extends views_handler_filter_date {

  /**
   * Overrides views_handler_filter_date::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['start_offset'] = array(
      'default' => 0,
    );

    return $options;
  }

  /**
   * Overrides views_handler_filter_date::option_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['start_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset in days'),
      '#description' => t('When to start the year of scope, e.g. -1 = yesterday, ...'),
      '#fieldset' => 'more',
      '#default_value' => $this->options['start_offset'],
    );

  }

  /**
   * Overrides views_handler_filter_date::op_simple().
   */
  function op_simple($field) {
    $value = intval(strtotime($this->value['value'], 0));
    if (!empty($this->value['type']) && $this->value['type'] == 'offset') {
      $value = '***CURRENT_TIME***' . sprintf('%+d', $value); // keep sign
    }

    $now = getdate(time() + 24 * 60 * 60 * intval($this->options['start_offset']));
    $real_field = $this->real_field;
    $formula = "CASE MONTH($real_field) > {$now['mon']} OR MONTH($real_field) = {$now['mon']} AND DAY($real_field) >= {$now['mday']}";
    $formula .= " WHEN TRUE THEN UNIX_TIMESTAMP(DATE(STR_TO_DATE(CONCAT_WS('-', {$now['year']}, MONTH($real_field), DAY($real_field),'00' ),'%Y-%m-%d-%H'))) $this->operator $value";
    $formula .= " ELSE UNIX_TIMESTAMP(DATE(STR_TO_DATE(CONCAT_WS('-', {$now['year']}+1, MONTH($real_field), DAY($real_field),'00' ),'%Y-%m-%d-%H')))  $this->operator $value END";

    // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when using an offset.
    $this->query->add_where_expression($this->options['group'], "$formula");

  }
  
  /**
   * Overrides views_handler_filter_date::op_between().
   */
  function op_between($field) {
    $a = intval(strtotime($this->value['min'], 0));
    $b = intval(strtotime($this->value['max'], 0));

    if ($this->value['type'] == 'offset') {
      $a = '***CURRENT_TIME***' . sprintf('%+d', $a); // keep sign
      $b = '***CURRENT_TIME***' . sprintf('%+d', $b); // keep sign
    }

    $operator = strtoupper($this->operator);

    $now = getdate(time() + 24 * 60 * 60 * intval($this->options['start_offset']));
    $real_field = $this->real_field;
    $formula = "CASE MONTH($real_field) > {$now['mon']} OR MONTH($real_field) = {$now['mon']} AND DAY($real_field) >= {$now['mday']}";
    $formula .= " WHEN TRUE THEN UNIX_TIMESTAMP(DATE(STR_TO_DATE(CONCAT_WS('-', {$now['year']}, MONTH($real_field), DAY($real_field),'00' ),'%Y-%m-%d-%H'))) $operator $a AND $b";
    $formula .= " ELSE UNIX_TIMESTAMP(DATE(STR_TO_DATE(CONCAT_WS('-', {$now['year']}+1, MONTH($real_field), DAY($real_field),'00' ),'%Y-%m-%d-%H'))) $operator $a AND $b END";

    // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when using an offset.
    $this->query->add_where_expression($this->options['group'], "$formula");
  }
  
}
